% load multitaper gamma power across subjects

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S14_Gamma/T_tools/']; addpath(pn.tools);
pn.savePath     = [pn.root, 'B_analyses/S14_Gamma/B_data/O_gammaPowBySub/'];
pn.plotFolder   = [pn.root, 'B_analyses/S14_Gamma/C_figures/O_gammaPowBySub/']; if ~exist(pn.plotFolder); mkdir(pn.plotFolder); end;
pn.dataOut = pn.savePath;

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs_num = sprintf('%s*', IDs{:});
IDs_num = sscanf(IDs_num, '%f*');

idxYA = IDs_num<2000;
idxOA = IDs_num>2000;

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);
    load([pn.savePath, IDs{id}, '_GammaMTM_v2.mat'], 'GammaByCond');
    GammaByCond2 = GammaByCond;
    cfg = [];
    %% temporal correction based on single-trial means
     % cf. Kloosterman:  We subtracted the trial-specific baseline value from
     % each sample in the time course per frequency bin and divided by the mean baseline
     % power across all trials within a session.

    allData = log10(cat(1,GammaByCond{1}.powspctrm,GammaByCond{2}.powspctrm,...
        GammaByCond{3}.powspctrm,GammaByCond{4}.powspctrm));
    idxTime = GammaByCond{1}.time > 2.2 & GammaByCond{1}.time<2.9;
    stData= repmat(nanmean(allData(:,:,:,idxTime),4),1,1,1,141);
    meanData= repmat(nanmean(nanmean(allData(:,:,:,idxTime),4),1),size(allData,1),1,1,141);
    stdData= repmat(nanstd(nanmean(allData(:,:,:,idxTime),4),[],1),size(allData,1),1,1,141);
    allData = (allData-stData)./meanData;
    
    trlOnsets = [0,cumsum([size(GammaByCond{1}.powspctrm,1),size(GammaByCond{2}.powspctrm,1),...
        size(GammaByCond{3}.powspctrm,1), size(GammaByCond{4}.powspctrm,1)])];
    for indCond = 1:4
        GammaByCond{indCond}.powspctrm = allData(trlOnsets(indCond)+1:trlOnsets(indCond+1),:,:,:);
    end
    % average across trials
    [GammaBySubCond{id,1}] = ft_freqdescriptives(cfg, GammaByCond{1});
    [GammaBySubCond{id,2}] = ft_freqdescriptives(cfg, GammaByCond{2});
    [GammaBySubCond{id,3}] = ft_freqdescriptives(cfg, GammaByCond{3});
    [GammaBySubCond{id,4}] = ft_freqdescriptives(cfg, GammaByCond{4});
    
    % save different variants in external structures
    
    Gamma = GammaBySubCond(id,:);
    % average within-condition baseline
    for indCond = 1:4
        timeMean = repmat(nanmean(Gamma{indCond}.powspctrm(:,:,idxTime),3),1,1,141);
        Gamma{indCond}.powspctrm2 = (Gamma{indCond}.powspctrm-timeMean);
    end
    save([pn.savePath, IDs{id}, '_GammaMTM_ST_mean_v2.mat'], 'Gamma')
    
    %% z-score variant
    GammaByCond = GammaByCond2;
    
    % calculate z-scores, based on grand mean
    
    allData = log10(cat(1,GammaByCond{1}.powspctrm,GammaByCond{2}.powspctrm,...
        GammaByCond{3}.powspctrm,GammaByCond{4}.powspctrm));
    idxTime = GammaByCond{1}.time > 2.2 & GammaByCond{1}.time<2.9;
    stData= repmat(nanmean(allData(:,:,:,idxTime),4),1,1,1,141);
    meanData= repmat(nanmean(nanmean(allData(:,:,:,idxTime),4),1),size(allData,1),1,1,141);
    stdData= repmat(nanstd(nanmean(allData(:,:,:,idxTime),4),[],1),size(allData,1),1,1,141);
    allData = (allData-meanData)./stdData; % remove grand mean, normalize by standard deviation across all trials
    
    trlOnsets = [0,cumsum([size(GammaByCond{1}.powspctrm,1),size(GammaByCond{2}.powspctrm,1),...
        size(GammaByCond{3}.powspctrm,1), size(GammaByCond{4}.powspctrm,1)])];
    for indCond = 1:4
        GammaByCond{indCond}.powspctrm = allData(trlOnsets(indCond)+1:trlOnsets(indCond+1),:,:,:);
    end
    % average across trials
    [GammaBySubCond{id,1}] = ft_freqdescriptives(cfg, GammaByCond{1});
    [GammaBySubCond{id,2}] = ft_freqdescriptives(cfg, GammaByCond{2});
    [GammaBySubCond{id,3}] = ft_freqdescriptives(cfg, GammaByCond{3});
    [GammaBySubCond{id,4}] = ft_freqdescriptives(cfg, GammaByCond{4});
    
    Gamma = GammaBySubCond(id,:);
    % average within-condition baseline
    for indCond = 1:4
        timeMean = repmat(nanmean(Gamma{indCond}.powspctrm(:,:,idxTime),3),1,1,141);
        Gamma{indCond}.powspctrm2 = (Gamma{indCond}.powspctrm-timeMean);
    end
    save([pn.savePath, IDs{id}, '_GammaMTM_zscore_v2.mat'], 'Gamma')
    
    %% original variant
    GammaByCond = GammaByCond2;
    
    % calculate z-scores, based on single-trial prestim mean
    
    allData = log10(cat(1,GammaByCond{1}.powspctrm,GammaByCond{2}.powspctrm,...
        GammaByCond{3}.powspctrm,GammaByCond{4}.powspctrm));
    idxTime = GammaByCond{1}.time > 2.2 & GammaByCond{1}.time<2.9;
    stData= repmat(nanmean(allData(:,:,:,idxTime),4),1,1,1,141);
    stData_sd= repmat(nanstd(allData(:,:,:,idxTime),[],4),1,1,1,141);
    meanData= repmat(nanmean(nanmean(allData(:,:,:,idxTime),4),1),size(allData,1),1,1,141);
    stdData= repmat(nanstd(nanmean(allData(:,:,:,idxTime),4),[],1),size(allData,1),1,1,141);
    allData = (allData-stData)./(stdData); % remove single-trial mean, normalize by standard deviation across all trials
    
    trlOnsets = [0,cumsum([size(GammaByCond{1}.powspctrm,1),size(GammaByCond{2}.powspctrm,1),...
        size(GammaByCond{3}.powspctrm,1), size(GammaByCond{4}.powspctrm,1)])];
    for indCond = 1:4
        GammaByCond{indCond}.powspctrm = allData(trlOnsets(indCond)+1:trlOnsets(indCond+1),:,:,:);
    end
    % average across trials
    [GammaBySubCond{id,1}] = ft_freqdescriptives(cfg, GammaByCond{1});
    [GammaBySubCond{id,2}] = ft_freqdescriptives(cfg, GammaByCond{2});
    [GammaBySubCond{id,3}] = ft_freqdescriptives(cfg, GammaByCond{3});
    [GammaBySubCond{id,4}] = ft_freqdescriptives(cfg, GammaByCond{4});
        
    Gamma = GammaBySubCond(id,:);
    % average within-condition baseline
    for indCond = 1:4
        timeMean = repmat(nanmean(Gamma{indCond}.powspctrm(:,:,idxTime),3),1,1,141);
        Gamma{indCond}.powspctrm2 = (Gamma{indCond}.powspctrm-timeMean);
    end
    
    save([pn.savePath, IDs{id}, '_GammaMTM_Zst_v2.mat'], 'Gamma')
        
    % WIP plotting
    
%     figure
%     hold on;
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(GammaBySubCond{id,1}.powspctrm(58:60,15:end,:),2),1))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(GammaBySubCond{id,2}.powspctrm(58:60,15:end,:),2),1))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(GammaBySubCond{id,3}.powspctrm(58:60,15:end,:),2),1))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(GammaBySubCond{id,4}.powspctrm(58:60,15:end,:),2),1))); xlabel('Time (s)'); ylabel('Frequency');
%     legend({'L1'; 'L2'; 'L3'; 'L4'})

    %     
%     figure
%     imagesc(squeeze(nanmean(nanmean(GammaByCond{indCond}.powspctrm(:,58:60,15:end,:),3),2))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(nanmean(GammaByCond{2}.powspctrm(:,58:60,15:end,:),3),2),4))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(nanmean(GammaByCond{3}.powspctrm(:,58:60,15:end,:),3),2),4))); xlabel('Time (s)'); ylabel('Frequency');
%     plot(GammaBySubCond{id,1}.time,squeeze(nanmean(nanmean(nanmean(GammaByCond{4}.powspctrm(:,58:60,15:end,:),3),2),4))); xlabel('Time (s)'); ylabel('Frequency');
%     legend({'L1'; 'L2'; 'L3'; 'L4'})
    
end

%% load normalized data

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);
    load([pn.savePath, IDs{id}, '_GammaMTM_zscore_v2.mat'], 'Gamma')
    GammaBySubCond{id,1} = Gamma{1,1};
    GammaBySubCond{id,2} = Gamma{1,2};
    GammaBySubCond{id,3} = Gamma{1,3};
    GammaBySubCond{id,4} = Gamma{1,4};
end

cfg = [];
cfg.parameter = 'powspctrm';
cfg.keepindividual = 'yes';
[GammaRaw{1,1}] = ft_freqgrandaverage(cfg, GammaBySubCond{:,1});
[GammaRaw{1,2}] = ft_freqgrandaverage(cfg, GammaBySubCond{:,2});
[GammaRaw{1,3}] = ft_freqgrandaverage(cfg, GammaBySubCond{:,3});
[GammaRaw{1,4}] = ft_freqgrandaverage(cfg, GammaBySubCond{:,4});

%% use another normalization step for the average (within-condition now)

allData = nanmean(cat(5,GammaRaw{1}.powspctrm,GammaRaw{2}.powspctrm,...
    GammaRaw{3}.powspctrm,GammaRaw{4}.powspctrm),5);

% figure; 
% subplot(2,2,1); imagesc(GammaRaw{indCond}.time,[],squeeze(nanmean(nanmean(allData(:,:,15:35,:),3),1))); xlabel('Time (s)'); ylabel('Channel');
% subplot(2,2,2); imagesc(GammaRaw{indCond}.time,GammaRaw{1,1}.freq,squeeze(nanmean(nanmean(GammaRaw{1,1}.powspctrm(:,58:60,:,:),2),1))); xlabel('Time (s)'); ylabel('Frequency');
% subplot(2,2,3); imagesc(GammaRaw{indCond}.time,[],squeeze(nanmean(nanmean(GammaRaw{1,1}.powspctrm(:,58:60,15:35,:),2),3)), [-.5 .5]); xlabel('Time (s)'); ylabel('Subject (L1)');
% subplot(2,2,4); imagesc(GammaRaw{indCond}.time,[],squeeze(nanmean(nanmean(allData(:,55:60,15:35,:),2),3)), [-.5 .5]); xlabel('Time (s)'); ylabel('Subject');
% suptitle('Single-trial normalized Gamma responses');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

for indCond = 1:4
    timeIdx = [find(GammaRaw{indCond}.time>=2 & GammaRaw{indCond}.time<2.9)];
    timeMean = repmat(nanmean(GammaRaw{indCond}.powspctrm(:,:,:,timeIdx),4),1,1,1,141);
    timeSTD = repmat(nanstd(GammaRaw{indCond}.powspctrm(:,:,:,timeIdx),[],4),1,1,1,141);
    GammaRaw{indCond}.powspctrm2 = (GammaRaw{indCond}.powspctrm-timeMean);
end
allData_norm = nanmean(cat(5,GammaRaw{1}.powspctrm2,GammaRaw{2}.powspctrm2,...
    GammaRaw{3}.powspctrm2,GammaRaw{4}.powspctrm2),5);

%% get separate YA and OA structures

for indCond = 1:4
    GammaRawYA{indCond} = GammaRaw{indCond};
    GammaRawYA{indCond}.powspctrm2 = GammaRaw{indCond}.powspctrm2(idxYA,:,:,:);
    GammaRawOA{indCond} = GammaRaw{indCond};
    GammaRawOA{indCond}.powspctrm2 = GammaRaw{indCond}.powspctrm2(idxOA,:,:,:);
end; %clear GammaRaw indCond

%% create Gamma matrix for further processing

% for indCond = 1:4
%     % YA
%     GammaMatrix_YA.pow(indCond,:,:,:,:) = GammaRawYA{indCond}.powspctrm2;
%     GammaMatrix_YA.time = GammaRawYA{1}.time;
%     GammaMatrix_YA.freq = GammaRawYA{1}.freq;
%     GammaMatrix_YA.label = GammaRawYA{1}.label;
%     GammaMatrix_YA.dimord = 'rpt_subj_chan_freq_time';
%     % OA
%     GammaMatrix_OA.pow(indCond,:,:,:,:) = GammaRawOA{indCond}.powspctrm2;
%     GammaMatrix_OA.time = GammaRawOA{1}.time;
%     GammaMatrix_OA.freq = GammaRawOA{1}.freq;
%     GammaMatrix_OA.label = GammaRawOA{1}.label;
%     GammaMatrix_OA.dimord = 'rpt_subj_chan_freq_time';
% end

% save([pn.savePath, 'GammaMatrix_YA_v2.mat'], 'GammaMatrix_YA')
% save([pn.savePath, 'GammaMatrix_OA_v2.mat'], 'GammaMatrix_OA')

%% load data

load([pn.savePath, 'GammaMatrix_YA_v2.mat'], 'GammaMatrix_YA')
load([pn.savePath, 'GammaMatrix_OA_v2.mat'], 'GammaMatrix_OA')

% plot load 1 gamma as a function of attentional modulation score

figure; hold on;
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(1,:,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)));
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(2:4,:,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2),1)));
xlabel('Time (s)'); ylabel('Amplitude'); title('Power change from baseline (avg)')

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

% N = 47 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

idx_AttFactor = find(ismember(EEGAttentionFactor.IDs, IDs));
[sortVal, sortIdx] = sort(EEGAttentionFactor.PCAalphaGamma(idx_AttFactor), 'ascend');

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

% lowChIdx = sortIdx(1:29);
% highChIdx = sortIdx(30:end);

figure; hold on;
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmedian(nanmean(GammaMatrix_YA.pow(1,lowChIdx,44:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'k', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmedian(nanmean(GammaMatrix_YA.pow(2:4,lowChIdx,44:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2),1)), 'k--', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmedian(nanmean(GammaMatrix_YA.pow(1,highChIdx,44:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'r', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmedian(nanmean(GammaMatrix_YA.pow(2:4,highChIdx,44:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2),1)), 'r--', 'LineWidth', 2);
xlabel('Time (s)'); ylabel('Channel'); title('Gamma power change from baseline (avg)')
legend({'Low change L1'; 'Low change L2-4'; 'High change L1'; 'High change L2-4'}, 'location', 'NorthWest')


figure; hold on;
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(1,highChIdx,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'r', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(2,highChIdx,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'r', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(3,highChIdx,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'r', 'LineWidth', 2);
plot(GammaMatrix_YA.time,squeeze(nanmean(nanmean(nanmean(GammaMatrix_YA.pow(4,highChIdx,55:60,GammaMatrix_YA.freq>60 & GammaMatrix_YA.freq<90,:),4),3),2)), 'r--', 'LineWidth', 2);
xlabel('Time (s)'); ylabel('Channel'); title('Gamma power change from baseline (avg)')
legend({'Low change L1'; 'Low change L2-4'; 'High change L1'; 'High change L2-4'}, 'location', 'NorthWest')


%% plots

figure; 
subplot(2,2,1); imagesc(GammaRawYA{indCond}.time,[],squeeze(nanmean(nanmean(allData_norm(:,:,15:35,:),3),1)), [-.1 .1]); colorbar; xlabel('Time (s)'); ylabel('Channel'); title('Power change from baseline (avg)')
subplot(2,2,2); imagesc(GammaRawYA{indCond}.time,GammaRawYA{1,1}.freq,squeeze(nanmean(nanmean(allData_norm(:,58:60,:,:),2),1)), [-.1 .1]); colorbar;  xlabel('Time (s)'); ylabel('Frequency'); title('Power change from baseline (avg)')
subplot(2,2,3); imagesc(GammaRawYA{indCond}.time,[],squeeze(nanmean(nanmean(GammaRawYA{1}.powspctrm2(:,55:60,15:35,:),2),3)), [-.3 .3]); colorbar; xlabel('Time (s)'); ylabel('Subject (L1)'); title('Individual gamma (L1)')
subplot(2,2,4); imagesc(GammaRawYA{indCond}.time,[],squeeze(nanmean(nanmean(allData_norm(:,55:60,15:35,:),2),3)), [-.3 .3]); colorbar; xlabel('Time (s)'); ylabel('Subject'); title('Individual gamma (avg)')
suptitle('Single-trial normalized Gamma responses');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure; 
subplot(1,2,1); hold on;
    plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawYA{1,1}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawYA{1,2}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawYA{1,3}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawYA{1,4}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    legend({'L1', 'L2', 'L3', 'L4'});
subplot(1,2,2); hold on;
    plot(GammaRawOA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawOA{1,1}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawOA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawOA{1,2}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawOA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawOA{1,3}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    plot(GammaRawOA{indCond}.time,squeeze(nanmean(nanmean(max(GammaRawOA{1,4}.powspctrm2(:,51:60,20:end,:),[],2),3),1)))
    legend({'L1', 'L2', 'L3', 'L4'});

figure; hold on;
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,1}.powspctrm2(:,51:60,20:end,:),2),3),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,2}.powspctrm2(:,51:60,20:end,:),2),3),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,3}.powspctrm2(:,51:60,20:end,:),2),3),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,4}.powspctrm2(:,51:60,20:end,:),2),3),1)))
legend({'L1', 'L2', 'L3', 'L4'}); xlabel('Time (s)'); ylabel('Gamma amplitude (norm.)')

%% plot by target load for YA

% average from 20 to 40 Hz @ occipital channels, where age difference is
% observed

freqIdx = find(GammaRawYA{indCond}.freq > 70 & GammaRawYA{indCond}.freq < 95);
timeIdx = find(GammaRawYA{indCond}.time > 3 & GammaRawYA{indCond}.time < 6);

dataToPlot = [];
for indCond = 1:4
    dataToPlot(1, indCond,:) = squeeze(nanmean(nanmedian(nanmedian(GammaRawYA{indCond}.powspctrm2(:,[58:60],freqIdx,timeIdx),2),4),3));
    %dataToPlot(2, indCond) = squeeze(nanmean(nanmean(nanmean(nanmean(GammaRawOA{indCond}.powspctrm(:,59,freqIdx,timeIdx),4),3),2),1));
end

[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,2,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,3,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,4,:))
[~, p] = ttest(dataToPlot(1,2,:), dataToPlot(1,4,:))

[~, p] = ttest(dataToPlot(1,1,:), nanmean(dataToPlot(1,2:4,:),2))


figure; imagesc(GammaRawYA{indCond}.time, GammaRawYA{indCond}.freq, squeeze(nanmedian(nanmean(GammaRawYA{indCond}.powspctrm2(:,[54,55,56,58:60],:,:),2),1)))

figure;
subplot(1,2,1);
    bar(nanmean(dataToPlot(1,:,:),3))
    xlabel('Load'); ylabel('Gamma magnitude (normalized)')
    title('Occipital Gamma during stim')
subplot(1,2,2);
    freqIdx = find(GammaRawYA{indCond}.freq > 70 & GammaRawYA{indCond}.freq < 95);
    timeIdx = find(GammaRawYA{indCond}.time > 6 & GammaRawYA{indCond}.time < 7);
    dataToPlot = [];
    for indCond = 1:4
        dataToPlot(1, indCond,:) = squeeze(nanmean(nanmedian(nanmedian(GammaRawYA{indCond}.powspctrm2(:,[58:60],freqIdx,timeIdx),2),4),3));
    end
    bar(nanmean(dataToPlot(1,:,:),3))
    xlabel('Load'); ylabel('Gamma magnitude (normalized)')
    title('Occipital Gamma during probe')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot by load for OA

freqIdx = find(GammaRawYA{indCond}.freq > 70 & GammaRawYA{indCond}.freq < 95);
timeIdx = find(GammaRawYA{indCond}.time > 3 & GammaRawYA{indCond}.time < 6);

dataToPlot = [];
for indCond = 1:4
    dataToPlot(1, indCond,:) = squeeze(nanmean(nanmedian(nanmedian(GammaRawOA{indCond}.powspctrm2(:,[58:60],freqIdx,timeIdx),2),4),3));
    %dataToPlot(2, indCond) = squeeze(nanmean(nanmean(nanmean(nanmean(GammaRawOA{indCond}.powspctrm(:,59,freqIdx,timeIdx),4),3),2),1));
end

[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,2,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,3,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,4,:))
[~, p] = ttest(dataToPlot(1,2,:), dataToPlot(1,4,:))

[~, p] = ttest(dataToPlot(1,1,:), nanmean(dataToPlot(1,2:4,:),2))


figure; imagesc(GammaRawOA{indCond}.time, GammaRawOA{indCond}.freq, squeeze(nanmedian(nanmean(GammaRawOA{indCond}.powspctrm2(:,[54,55,56,58:60],:,:),2),1)))

figure;
subplot(1,2,1);
    bar(nanmean(dataToPlot(1,:,:),3))
    xlabel('Load'); ylabel('Gamma magnitude (normalized)')
    title('Occipital Gamma during stim')
subplot(1,2,2);
    freqIdx = find(GammaRawOA{indCond}.freq > 70 & GammaRawOA{indCond}.freq < 95);
    timeIdx = find(GammaRawOA{indCond}.time > 6 & GammaRawOA{indCond}.time < 7);
    dataToPlot = [];
    for indCond = 1:4
        dataToPlot(1, indCond,:) = squeeze(nanmean(nanmedian(nanmedian(GammaRawOA{indCond}.powspctrm2(:,[58:60],freqIdx,timeIdx),2),4),3));
    end
    bar(nanmean(dataToPlot(1,:,:),3))
    xlabel('Load'); ylabel('Gamma magnitude (normalized)')
    title('Occipital Gamma during probe')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% CBPA with 

time = GammaRawYA{1}.time;
freq = GammaRawYA{1}.freq;

%% YA

GammaRawYA{1,5} = GammaRawYA{1,1};
GammaRawYA{1,5}.powspctrm = squeeze(nanmean(cat(5, GammaRawYA{1,1}.powspctrm2, ...
    GammaRawYA{1,2}.powspctrm2, GammaRawYA{1,3}.powspctrm2, ...
    GammaRawYA{1,4}.powspctrm2),5));

% create 234 average
GammaRawYA{1,6} = GammaRawYA{1,1};
GammaRawYA{1,6}.powspctrm = squeeze(nanmean(cat(5, ...
    GammaRawYA{1,2}.powspctrm2, GammaRawYA{1,3}.powspctrm2, GammaRawYA{1,4}.powspctrm2),5));


baseline_time = find(GammaRawYA{1,1}.time>2.5 & GammaRawYA{1,1}.time < 2.8);

zeroData_YA = GammaRawYA{1,5};
zeroData_YA.powspctrm = repmat(squeeze(nanmean(zeroData_YA.powspctrm2(:,:,:,baseline_time),4)),1,1,1,numel(time));
%zeroData_YA.powspctrm = zeros(size(zeroData_YA.powspctrm2));


load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat', 'elec');

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      =  elec.label;

stat = [];

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_actvsblT';
% cfgStat.latency          = [3 6];
% cfgStat.avgovertime      = 'yes';
% cfgStat.avgoverfreq      = 'yes';
% cfgStat.frequency        = [70, 95];
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, zeroData_YA);

subj = size(zeroData_YA.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indC = 1:conds
for i = 1:subj
    design(1,(indC-1)*subj+i) = indC;
    design(2,(indC-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

for indCond = 1:5
    [stat{1, indCond}] = ft_freqstatistics(cfgStat, GammaRawYA{1,indCond}, zeroData_YA);
end

save([pn.dataOut, 'O2_YAvsBL_singleTrialZ.mat'], 'stat', 'cfgStat');

% figure; imagesc(squeeze(stat{1, 5}.mask.*stat{1,5}.stat))
% figure; imagesc(squeeze(stat{1,5}.stat))
figure; 
subplot(1,3,1);
    imagesc(time,freq,squeeze(nanmean(stat{1,5}.mask(55:60,:,:).*stat{1,5}.stat(55:60,:,:),1)))
    xlabel('Time (s)'); ylabel('Frequency (Hz)')
subplot(1,3,2);
    imagesc(time,[],squeeze(nanmean(stat{1,5}.mask(:,20:35,:).*stat{1,5}.stat(:,20:35,:),2)))
    xlabel('Time (s)'); ylabel('Channel')

% topoplot

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
cfg.highlightchannel = stat{1,5}.label(nanmean(nanmean(stat{1,5}.mask(:,20:35,time>3 & time<6),2),3)>0);

subplot(1,3,3);
plotData = [];
plotData.label = stat{1,5}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(nanmean(stat{1,5}.stat(:,20:35,time>3 & time<6),2),3));
ft_topoplotER(cfg,plotData);
title('YA: Gamma across conditions vs BL')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

gammaChans = find(stat{1,5}.mask);

freqIdx = find(GammaRawYA{indCond}.freq > 70 & GammaRawYA{indCond}.freq < 95);

dataToPlot = [];
for indCond = 1:4
    dataToPlot(1, indCond,:,:) = squeeze(nanmean(nanmean(GammaRawYA{indCond}.powspctrm(:,gammaChans,freqIdx,:),2),3));
end

figure; imagesc(squeeze(nanmean(dataToPlot(1,:,:,:),2)))

figure; hold on;
plot(squeeze(nanmean(nanmean(dataToPlot(1,1,:,:),2),3)))
plot(squeeze(nanmean(nanmean(dataToPlot(1,2,:,:),2),3)))
plot(squeeze(nanmean(nanmean(dataToPlot(1,3,:,:),2),3)))
plot(squeeze(nanmean(nanmean(dataToPlot(1,4,:,:),2),3)))

%% OA

% GammaRawOA{1,5} = GammaRawOA{1,1};
% GammaRawOA{1,5}.powspctrm = squeeze(nanmean(cat(5, GammaRawOA{1,1}.powspctrm, ...
%     GammaRawOA{1,2}.powspctrm, GammaRawOA{1,3}.powspctrm, ...
%     GammaRawOA{1,4}.powspctrm),5));
% 
% baseline_time = find(GammaRawOA{1,1}.time>2.6 & GammaRawOA{1,1}.time < 3);
% 
% zeroData_OA = GammaRawOA{1,5};
% zeroData_OA.powspctrm = repmat(squeeze(nanmean(zeroData_OA.powspctrm(:,:,:,baseline_time),4)),1,1,1,numel(time));
% 
% subj = size(zeroData_OA.powspctrm,1);
% conds = 2;
% design = zeros(2,conds*subj);
% for iC = 1:conds
% for i = 1:subj
%     design(1,(iC-1)*subj+i) = iC;
%     design(2,(iC-1)*subj+i) = i;
% end
% end
% cfgStat.design   = design;
% cfgStat.ivar     = 1;
% cfgStat.uvar     = 2;
% 
% stat = [];
% for indCond = 1:5
%     [stat{1, indCond}] = ft_freqstatistics(cfgStat, GammaRawOA{1,indCond}, zeroData_OA);
% end
%     
% save([pn.dataOut, 'O2_OAvsBL.mat'], 'stat');

%% YA vs OA

% stat = [];
% 
% for indCond = 1:5
%     cfgStat = [];
%     cfgStat.channel          = 'all';
%     cfgStat.method           = 'montecarlo';
%     cfgStat.statistic        = 'ft_statfun_indepsamplesT';
%     cfgStat.correctm         = 'cluster';
%     cfgStat.clusteralpha     = 0.05;
%     cfgStat.clusterstatistic = 'maxsum';
%     cfgStat.minnbchan        = 2;
%     cfgStat.tail             = 0;
%     cfgStat.clustertail      = 0;
%     cfgStat.alpha            = 0.025;
%     cfgStat.numrandomization = 500;
%     cfgStat.parameter        = 'powspctrm';
%     cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, GammaRawYA{5});
% 
%     N_1 = size(GammaRawOA{5}.powspctrm,1);
%     N_2 = size(GammaRawYA{5}.powspctrm,1);
%     design = zeros(2,N_1+N_2);
%     design(1,1:N_1) = 1;
%     design(1,N_1+1:end) = 2;
% 
%     cfgStat.design   = design;
%     cfgStat.ivar     = 1;
% 
%     [stat{1, indCond}] = ft_freqstatistics(cfgStat, GammaRawOA{indCond}, GammaRawYA{indCond});
% end
% 
% save([pn.dataOut, 'O2_YAvsOA.mat'], 'stat');

%% depregrT: YA

stat = [];

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, GammaRawYA{1});

subj = size(GammaRawYA{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indC = 1:conds
for i = 1:subj
    design(1,(indC-1)*subj+i) = indC;
    design(2,(indC-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat{1, 1}] = ft_freqstatistics(cfgStat, GammaRawYA{1}, GammaRawYA{2},...
    GammaRawYA{3}, GammaRawYA{4});

% test 1 vs 234

subj = size(GammaRawYA{1}.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indC = 1:conds
for i = 1:subj
    design(1,(indC-1)*subj+i) = indC;
    design(2,(indC-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat{1, 2}] = ft_freqstatistics(cfgStat, GammaRawYA{1}, GammaRawYA{6});

% curvilinear association

subj = size(GammaRawYA{1}.powspctrm,1);
condLabels = [1,4,3,2];
conds = 4;
design = zeros(2,conds*subj);
for indC = 1:conds
for i = 1:subj
    design(1,(indC-1)*subj+i) = condLabels(indC);
    design(2,(indC-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat{1, 3}] = ft_freqstatistics(cfgStat, GammaRawYA{1}, GammaRawYA{2},...
    GammaRawYA{3}, GammaRawYA{4});

save([pn.dataOut, 'O2_YAdepregrT_singleTrialZ.mat'], 'stat', 'cfgStat');

figure; 
subplot(1,3,1);
    imagesc(time,freq,squeeze(nanmean(stat{1,1}.mask(55:60,:,:).*stat{1,1}.stat(55:60,:,:),1)))
    xlabel('Time (s)'); ylabel('Frequency (Hz)')
subplot(1,3,2);
    imagesc(time,[],squeeze(nanmean(stat{1,1}.mask(:,20:35,:).*stat{1,1}.stat(:,20:35,:),2)))
    xlabel('Time (s)'); ylabel('Channel')

% topoplot

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
cfg.highlightchannel = stat{1,1}.label(nanmean(nanmean(stat{1,1}.mask(:,20:35,time>3 & time<6),2),3)>0);

subplot(1,3,3);
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(nanmean(stat{1,1}.stat(:,20:35,time>3 & time<6),2),3));
ft_topoplotER(cfg,plotData);
title('YA: Gamma parametric load effect')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot probe effect

figure; 
subplot(1,3,1);
    imagesc(time,freq,squeeze(nanmean(stat{1,2}.mask(55:60,:,:).*stat{1,2}.stat(55:60,:,:),1)))
    xlabel('Time (s)'); ylabel('Frequency (Hz)')
subplot(1,3,2);
    imagesc(time,[],squeeze(nanmean(stat{1,2}.mask(:,20:35,:).*stat{1,2}.stat(:,20:35,:),2)))
    xlabel('Time (s)'); ylabel('Channel')
pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
cfg.highlightchannel = stat{1,1}.label(nanmean(nanmean(stat{1,2}.mask(:,20:35,time>6 & time<7),2),3)>0);

subplot(1,3,3);
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(nanmean(stat{1,2}.stat(:,20:35,time>6 & time<7),2),3));
ft_topoplotER(cfg,plotData);
title('YA: Gamma parametric load effect (probe)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot nonlinear association

figure; 
subplot(1,3,1);
    imagesc(time,freq,squeeze(nanmean(stat{1,3}.stat(55:60,:,:),1)))
    xlabel('Time (s)'); ylabel('Frequency (Hz)')
subplot(1,3,2);
    imagesc(time,[],squeeze(nanmean(stat{1,3}.stat(:,20:35,:),2)))
    xlabel('Time (s)'); ylabel('Channel')
pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
cfg.highlightchannel = stat{1,1}.label(nanmean(nanmean(stat{1,3}.mask(:,20:35,time>3 & time<6),2),3)>0);

subplot(1,3,3);
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(nanmean(stat{1,3}.stat(:,20:35,time>3 & time<6),2),3));
ft_topoplotER(cfg,plotData);
title('YA: Gamma parametric load effect (probe)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% depregrT: OA
% 
% stat = [];
% 
% cfgStat = [];
% cfgStat.channel          = 'all';
% cfgStat.method           = 'montecarlo';
% cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
% cfgStat.correctm         = 'cluster';
% cfgStat.clusteralpha     = 0.05;
% cfgStat.clusterstatistic = 'maxsum';
% cfgStat.minnbchan        = 3;
% cfgStat.tail             = 0;
% cfgStat.clustertail      = 0;
% cfgStat.alpha            = 0.025;
% cfgStat.numrandomization = 500;
% cfgStat.parameter        = 'powspctrm';
% cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, GammaRawOA{1});
% 
% subj = size(GammaRawOA{1}.powspctrm,1);
% conds = 4;
% design = zeros(2,conds*subj);
% for indCond = 1:conds
% for i = 1:subj
%     design(1,(indCond-1)*subj+i) = indCond;
%     design(2,(indCond-1)*subj+i) = i;
% end
% end
% cfgStat.design   = design;
% cfgStat.ivar     = 1;
% cfgStat.uvar     = 2;
% 
% [stat{1, 1}] = ft_freqstatistics(cfgStat, GammaRawOA{1}, GammaRawOA{2},...
%     GammaRawOA{3}, GammaRawOA{4});
% 
% save([pn.dataOut, 'O2_OAdepregrT.mat'], 'stat');

%% depregrT: YA (with averaging)

stat = [];

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.latency          = [3,6];
cfgStat.avgovertime      = 'yes';
cfgStat.freq             = [60, 95];
cfgStat.avgoverfreq      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, GammaRawYA{1});

subj = size(GammaRawYA{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indC = 1:conds
for i = 1:subj
    design(1,(indC-1)*subj+i) = indC;
    design(2,(indC-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat{1, 1}] = ft_freqstatistics(cfgStat, GammaRawYA{1}, GammaRawYA{2},...
    GammaRawYA{3}, GammaRawYA{4});

figure; imagesc(stat{1, 1}.stat.*stat{1, 1}.mask)

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
cfg.highlightchannel = stat{1,1}.label(stat{1,1}.mask);

figure;
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(stat{1,1}.stat);
ft_topoplotER(cfg,plotData);
title('YA: Gamma parametric load effect (stim)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

idx_chan = find(stat{1,1}.mask);

%% get channels from effects cluster and plot post-hoc load effect

freqIdx = find(GammaRawYA{indCond}.freq > 60 & GammaRawYA{indCond}.freq < 95);
timeIdx = find(GammaRawYA{indCond}.time > 3 & GammaRawYA{indCond}.time < 6);

dataToPlot = [];
for indCond = 1:4
    dataToPlot(1, indCond,:) = squeeze(max(nanmean(nanmean(GammaRawYA{indCond}.powspctrm2(:,idx_chan,freqIdx,timeIdx),4),3),[],2));
end

[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,2,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,3,:))
[~, p] = ttest(dataToPlot(1,1,:), dataToPlot(1,4,:))
[~, p] = ttest(dataToPlot(1,2,:), dataToPlot(1,4,:))

[~, p] = ttest(dataToPlot(1,1,:), nanmean(dataToPlot(1,2:4,:),2))


figure;
bar(nanmean(dataToPlot(1,:,:),3))
xlabel('Load'); ylabel('Gamma magnitude (normalized)')
title('Occipital Gamma during stim')


figure; hold on;
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,1}.powspctrm2(:,idx_chan,20:25,:),3),2),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,2}.powspctrm2(:,idx_chan,20:25,:),3),2),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,3}.powspctrm2(:,idx_chan,20:25,:),3),2),1)))
plot(GammaRawYA{indCond}.time,squeeze(nanmean(nanmean(nanmean(GammaRawYA{1,4}.powspctrm2(:,idx_chan,20:25,:),3),2),1)))
legend({'L1', 'L2', 'L3', 'L4'}); xlabel('Time (s)'); ylabel('Gamma amplitude (norm.)')
