% Calculate gamma Power by BPF (40-140 Hz) + Hilbert amplitude

% - no CSD-transform
% - 500 Hz sampling rate
% - average mastoid reference

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S14_Gamma/T_tools/']; addpath(pn.tools);
pn.savePath     = [pn.root, 'B_analyses/S14_Gamma/B_data/O_gammaPowBySub/'];

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    TrlInfo = data.TrlInfo;
   
    %% apply CSD transform
        
    % CSD transform
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo;
    
    %% split into conditions
    
    for indCond = 1:4
        cfg = [];
        cfg.trials = TrlInfo(:,8)==indCond;
        [dataByCond{indCond}] = ft_selectdata(cfg, data);
    end; clear indCond;
    
    %% Multitaper
    
    % cf. Kloosterman (2018)
    % step size, 200 ms; window length, 400 ms; frequency resolution, 2.5 Hz)
    % 20 - 100 Hz frequency range (spectral smoothing, 8 Hz; five tapers dpss)
    
    for indCond = 1:4
        cfg = [];
        cfg.method = 'mtmconvol';
        cfg.keeptapers  = 'no';
        cfg.taper = 'dpss'; % high frequency-optimized analysis (smooth)
        cfg.foi = 20:2:90;
        cfg.tapsmofrq = ones(length(cfg.foi),1) .* 8;
        cfg.t_ftimwin = ones(length(cfg.foi),1) .* 0.4;
        cfg.toi = 1:.05:8;
        cfg.keeptrials = 'yes';
        cfg.pad = 'nextpow2';
        [GammaByCond{indCond}] = ft_freqanalysis(cfg, dataByCond{indCond});
    end
    
     %% normalize by pre-stim interval
     % cf. Kloosterman:  We subtracted the trial-specific baseline value from
     % each sample in the time course per frequency bin and divided by the mean baseline
     % power across all trials within a session.

     for indCond = 1:4
        GammaByCond_PreStimNorm{indCond} = GammaByCond{indCond};
        PreStimIDX = find(GammaByCond{indCond}.time >= 2.5 & GammaByCond{indCond}.time <=2.8);
        PreStimPowByTrial = squeeze(nanmean(GammaByCond{indCond}.powspctrm(:,:,:,PreStimIDX),4));
        PreStimPowAvg = squeeze(nanmean(PreStimPowByTrial,1));
        Ntime = size(GammaByCond{indCond}.powspctrm,4);
        Ntrial = size(GammaByCond{indCond}.powspctrm,1);
        GammaByCond_PreStimNorm{indCond}.powspctrm = (GammaByCond{indCond}.powspctrm-...
            repmat(PreStimPowByTrial,1,1,1,Ntime))./...
            permute(repmat(PreStimPowAvg,1,1,Ntrial,Ntime),[3,1,2,4]);
        GammaByCond_PreStimNorm{indCond}.baseline = [2.5, 2.8];
     end
    
     %% normalize by average across all channels
     
%      for indCond = 1:4
%         PreStimIDX = find(GammaByCond{indCond}.time >= 2 & GammaByCond{indCond}.time <=2.8);
%         PreStimPowByTrial = squeeze(nanmean(GammaByCond{indCond}.powspctrm(:,:,:,PreStimIDX),4));
%         PreStimPowByTrial = PreStimPowByTrial-repmat(nanmean(PreStimPowByTrial,2),1,60,1);
%         PreStimPowAvg = squeeze(nanmean(PreStimPowByTrial,1));
%         Ntime = size(GammaByCond{indCond}.powspctrm,4);
%         Ntrial = size(GammaByCond{indCond}.powspctrm,1);
%         curPowspctrm = GammaByCond{indCond}.powspctrm-repmat(nanmean(GammaByCond{indCond}.powspctrm,2),1,60,1,1);
%         GammaByCond{indCond}.powspctrmPreStimNorm = (curPowspctrm-...
%             repmat(PreStimPowByTrial,1,1,1,Ntime))./...
%             permute(repmat(PreStimPowAvg,1,1,Ntrial,Ntime),[3,1,2,4]);
%      end
     
     
     %% save output data
     
     if ~exist(pn.savePath); mkdir(pn.savePath); end
     save([pn.savePath, IDs{id}, '_GammaMTM_v2.mat'], 'GammaByCond', 'GammaByCond_PreStimNorm')
     
    %% plot data
    
%     figure;
%     for indCond = 1:4
%         subplot(2,2,indCond)
%         channels = 44:60;
%         time = GammaByCond{indCond}.time;
%         freq = GammaByCond{indCond}.freq;
%         data = squeeze(nanmean(nanmean(GammaByCond{indCond}.powspctrmPreStimNorm(:,channels,:,:),1),2));
%         imagesc(time, freq, 100.*data, [-20 20]);
%         xlim([2.6, 6.5])
%     end
%     
%     figure;
%     for indCond = 1:3
%         subplot(2,2,indCond)
%         channels = 53:55;
%         time = GammaByCond{indCond}.time;
%         freq = GammaByCond{indCond}.freq;
%         data = squeeze(nanmean(nanmean(GammaByCond{indCond}.powspctrmPreStimNorm(:,channels,:,:),1),2));
%         imagesc(time, freq, 100.*data, [-20 20]);
%     end
% 
%     figure;
%     channels = 53:55;
%     channels = 55:60;
%     data = squeeze(nanmean(nanmean(GammaByCond_PreStimNorm{1}.powspctrm(:,channels,:,:),1),2));
%     %data = squeeze(nanmean(nanmean(GammaByCond{indCond}.powspctrm(:,channels,:,:),1),2));
%     imagesc(time, freq, data);

% figure;
% channels = 44:60;
% time = GammaByCond{indCond}.time;
% freq = GammaByCond{indCond}.freq;
% dataMerged = cat(4,squeeze(nanmean(GammaByCond{1}.powspctrmPreStimNorm,1)),...
%     squeeze(nanmean(GammaByCond{2}.powspctrmPreStimNorm,1)), ...
%     squeeze(nanmean(GammaByCond{3}.powspctrmPreStimNorm,1)), ...
%     squeeze(nanmean(GammaByCond{4}.powspctrmPreStimNorm,1)));
% dataMerged = squeeze(nanmean(dataMerged,4));
% data = squeeze(nanmean(dataMerged(channels,:,:),1));
% imagesc(time, freq, 100.*data, [-10 10]);
% xlim([2.6, 6.5])

end